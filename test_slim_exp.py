import read_data as tts
import os
import time
from model_slim import *
from test import test_model_slim
from globals import *


def evaluate(testset):

    num_categories = len(FLAGS.all_labels)

    dataset_test = tf.data.TFRecordDataset([f[0] for f in testset])
    dataset_test = dataset_test. \
        map(tts.dataset_map_tfr_multi, num_parallel_calls=4). \
        prefetch(FLAGS.train_batch_size * 3). \
        flat_map(lambda *x: tf.data.Dataset.from_tensor_slices(x)). \
        batch(FLAGS.test_batch_size)

    iterator_test = dataset_test.make_initializable_iterator()
    test_x,test_y = iterator_test.get_next()

    with slim.arg_scope(kaiming_residual_parametes(weight_decay=0.0004, batch_norm_decay=0.997)):
        test_logits, test_predictions = kaiming_residual(test_x, num_categories, reuse=False, is_training=False, n_filt=6, n_res_lay=2, layer1_size=6, use_max_pool=True, use_avg_pool=False)

    test_ncorrect = accuracy(test_predictions, test_y)
    test_predidx = tf.argmax(test_logits, 1)
    #test_cm = tf.confusion_matrix(test_y, test_predidx, num_classes=num_categories, name='test_cm' )

    names_to_values, names_to_updates = slim.metrics.aggregate_metric_map({
        'eval/Count': tf.contrib.metrics.count(test_y),
        'eval/Accuracy': tf.metrics.accuracy(test_y, test_predidx),
        'eval/PerClass': tf.metrics.mean_per_class_accuracy(test_y, test_predidx,num_categories),
        'eval/ConfusionT': sum_tensor(tf.confusion_matrix(test_y, test_predidx, num_categories))

    })

    summary_ops = []
    for metric_name, metric_value in names_to_values.items():
        if (not metric_name.endswith('T')):
            op = tf.summary.scalar(metric_name, metric_value)
        else:
            op = tf.summary.tensor_summary(metric_name, metric_value)
        op = tf.Print(op, [metric_value], metric_name)
        summary_ops.append(op)

    config = tf.ConfigProto(device_count={'GPU': 0})

    once = True
    if once:
        checkpoint_path = tf.train.latest_checkpoint(FLAGS.log_dir)
        metric_values = slim.evaluation.evaluate_once(
            '',
            checkpoint_path,
            logdir=FLAGS.log_dir, # for once
            num_evals=100,
            initial_op=iterator_test.initializer,
            eval_op=[x for x in names_to_updates.values()],
            summary_op=tf.summary.merge(summary_ops),
            hooks=[AfterCreateSessionHook(iterator_test.initializer)],
            final_op=[ x for x in names_to_values.values()],
            session_config=config
        )
    else:
        metric_values = slim.evaluation.evaluation_loop(
            '',
            FLAGS.log_dir,
            FLAGS.log_dir, # for loop
            num_evals=100,
            initial_op=iterator_test.initializer,
            eval_op=[x for x in names_to_updates.values()],
            summary_op=tf.summary.merge(summary_ops),
            hooks=[AfterCreateSessionHook(iterator_test.initializer)],
            final_op=[ x for x in names_to_values.values()],
            session_config=config
        )

    names_to_values = dict(zip(names_to_values.keys(), metric_values))
    for name in names_to_values:
        print('%s: ' % (name))
        print(names_to_values[name])




class AfterCreateSessionHook(tf.train.SessionRunHook):
  """A run hook that saves a summary with the results of evaluation."""

  def __init__(self,
               init_op):
    self.init_op = init_op

  def after_create_session(self, session, coord):
      session.run([self.init_op])


def sum_tensor(values):

  with tf.name_scope('sum'):
    total =  tf.Variable(
            lambda: tf.zeros(values.get_shape(), tf.int32),
            trainable=False,
            collections=[ ops.GraphKeys.LOCAL_VARIABLES, ops.GraphKeys.METRIC_VARIABLES ],
            validate_shape=True,
            name='total')

    update_total_op = tf.assign_add(total, values)

    return total, update_total_op

def main(_):
    global all_labels

    if tf.gfile.Exists(FLAGS.train_dir):
        tf.gfile.DeleteRecursively(FLAGS.train_dir)
    #tf.gfile.MakeDirs(FLAGS.train_dir)

    trainset, testset, all_labels = tts.train_test_set(['1', '2', 'i', 'b', 's'], 0.33, ['effects', 'gtzan', 'mirex2015', 'gni'], FLAGS.num_samples, FLAGS.do_mel)

    FLAGS.all_labels = all_labels

    evaluate(testset)


if __name__ == '__main__':
    tf.app.run()

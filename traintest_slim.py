import read_data as tts
import tempfile
from model_slim import *
from globals import *
from tensorflow.contrib.slim.python.slim.learning import train_step
from test_slim_exp import sum_tensor
import tensorflow.contrib.slim as slim

def train(trainset, testset):

    num_categories = len(FLAGS.all_labels)

    
    tf.reset_default_graph()

    dataset_train = tf.data.TFRecordDataset([f[0] for f in trainset])
    dataset_train = dataset_train.\
        map(tts.dataset_map_tfr_single, num_parallel_calls=4 ).\
        shuffle(FLAGS.fetch_buffer_size).\
        batch(FLAGS.train_batch_size).\
        repeat(FLAGS.n_epochs) # batch size -> kok mreza spusti skoz preden poravlja parametre, da ne overfita prevec (google batch size 8 - odvisen od mreže) --> train_batch_size

    dataset_test = tf.data.TFRecordDataset([f[0] for f in testset])
    dataset_test = dataset_test. \
        map(tts.dataset_map_tfr_multi, num_parallel_calls=4). \
        prefetch(FLAGS.train_batch_size * 3). \
        flat_map(lambda *x: tf.data.Dataset.from_tensor_slices(x)). \
        batch(FLAGS.test_batch_size)

    iterator_train = dataset_train.make_initializable_iterator()
    x,y = iterator_train.get_next()        
    iterator_test = dataset_test.make_initializable_iterator() # za izpis session pa get pa to
    test_x,test_y = iterator_test.get_next()

    #za začetek strašno simple model, pogledat kako se naredi navadna nevronska mreža s hidden layerjem, outputov toliko kolikor je number of outputs (kolikor je različnih classov)
    #with slim.arg_scope(kaiming_residual_parametes(weight_decay=0.00004, batch_norm_decay=0.9997, batch_norm_epsilon=0.001)): #kaiming residual
    #    logits, predictions = kaiming_residual(x, num_categories, is_training=True,                             n_filt=6, n_res_lay=4, layer1_size=8, use_max_pool=var, use_avg_pool=False)
    #    test_logits, test_predictions = kaiming_residual(test_x, num_categories, reuse=True, is_training=False, n_filt=6, n_res_lay=4, layer1_size=8, use_max_pool=var, use_avg_pool=False)
    
    x = tf.slice(x, [0, 0, 1, 0], [-1, -1, -1, -1])
    x = tf.reshape(x, [-1, 229])
    x = slim.fully_connected(x, 60)
    x = slim.fully_connected(x, 4)    
    predictions = tf.nn.softmax(x, name="softmax_tensor")
    #predictions = tf.argmax(input=x, axis = 0)

    
    test_x = tf.reshape(test_x, [-1, 229])
    test_x = slim.fully_connected(test_x, 60)
    test_x = slim.fully_connected(test_x, 4)    
    test_predictions = tf.nn.softmax(test_x, name="softmax_tensor")

    one_hot_labels = slim.one_hot_encoding(y, num_categories) # slim ma funkcijo za one hot encoding :) --> y je indeks v arrayu labelov, 1 hot je pa velik toliko kolikor je kategorij, pa enko ma na tem mestu
    
    loss = tf.losses.softmax_cross_entropy(one_hot_labels, logits=predictions)    #  logits) # loss.jpg
    ncorrect = accuracy(predictions, y) # kok smo jih zadel

    test_predidx = tf.argmax(test_predictions,axis=1) # not sure?
    names_to_values, names_to_updates = slim.metrics.aggregate_metric_map({
        'eval/Count': tf.contrib.metrics.count(test_y),
        'eval/Accuracy': tf.metrics.accuracy(test_y, test_predidx),
        'eval/PerClass': tf.metrics.mean_per_class_accuracy(test_y, test_predidx,num_categories),
        'eval/ConfusionMT': sum_tensor(tf.confusion_matrix(test_y, test_predidx, num_categories)) # parametri uspesnosti pa to

    })
    
    summary_ops = [] # neki povzetki,           sUMMARYJE NACELOMA LAHKO VEN
    for metric_name, metric_value in names_to_values.items():
        if (not metric_name.endswith('T')):
            op = tf.summary.scalar(metric_name, metric_value)
        else:
            cmi = tf.reshape( tf.cast( metric_value, tf.float32),[1, num_categories, num_categories, 1])
            op = tf.summary.image(metric_name, cmi)
        op = tf.Print(op, [metric_value], metric_name)
        summary_ops.append(op)
    # podatke lahko shranjuje na disk, to prikazuje v tensorboardu
    for var in [x for x in tf.get_collection(tf.GraphKeys.MODEL_VARIABLES) ]:
        summary_ops.append(tf.summary.histogram(var.op.name, predictions)) # tuki so zraven vse spremenljivke mreže (vsi histogrami)

    total_summary = tf.summary.merge(summary_ops)

    train_step_fn.iterator_test=iterator_test
    train_step_fn.accuracy = ncorrect
    train_step_fn.names_to_values  = names_to_values
    train_step_fn.names_to_updates = names_to_updates

    # The total loss is the users's loss plus any regularization losses.
    total_loss = tf.losses.get_total_loss(add_regularization_losses=True) # da ni prevelikih utezi, penalizira prevelike utezi v mrezi


    global_step = tf.train.get_or_create_global_step()

    learning_rate = tf.train.exponential_decay(0.1, global_step,
                                                500, 0.75, staircase=True)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)


    train_op = slim.learning.create_train_op(total_loss, optimizer, global_step=global_step)

    slim.learning.train(
        train_op,
        FLAGS.log_dir,
        local_init_op=tf.group(iterator_train.initializer,tf.local_variables_initializer()),
        log_every_n_steps=100,
        save_summaries_secs=100,
        save_interval_secs=300,
        train_step_fn=train_step_fn)


def train_step_fn(session, train_op, global_step, kwargs):
    total_loss, should_stop = train_step(session, train_op, global_step, kwargs)
    step = session.run(global_step)
    if step % 100 == 0:
        accuracy = session.run([train_step_fn.accuracy])
        print(step, accuracy, total_loss)
    if step % 1000 == 0:
        test_step(session, train_step_fn.names_to_values, train_step_fn.names_to_updates, train_step_fn.iterator_test, train_step_fn.total_summary)

    return [total_loss, should_stop]


def test_step(session, names_to_values, names_to_updates, iterator_test, total_summary):

    session.run(iterator_test.initializer)
    for var in session.graph.get_collection(ops.GraphKeys.METRIC_VARIABLES):
        session.run(var.initializer)

    while True:
        try:
            session.run(list(names_to_updates.values()))

        except tf.errors.OutOfRangeError:
            print('Done testing.')
            break

    session.run(total_summary)
    vals=session.run(list(names_to_values.values()))

    for name, val in zip(names_to_values.keys(), vals):
        print('%s: ' % (name))
        print(val)


def main(_):
    global all_labels
    print('akcija!')
    tf.gfile.MakeDirs(FLAGS.log_dir)
    with tempfile.TemporaryDirectory(prefix='run',dir=FLAGS.log_dir) as tmpdirname:
        FLAGS.log_dir=tmpdirname


    folders = ['audioData']
    # classes = range(0, 89)
    trainset, testset, all_labels = tts.train_test_set(['33', '45', '57', '69'], 0.2, folders, FLAGS.num_samples)


    FLAGS.all_labels = all_labels

    train(trainset, testset)


if __name__ == '__main__':
    tf.app.run()

import numpy as np
import librosa
from read_data import dataset_files_labels, write_features
from globals import *
from audioset import mel_features

def calc_features(fileName):
    #tensorflow ma funkcijo za 1-hot encoding
    # network da neki pozre pa traina
    y, sr = librosa.load(fileName, sr=16000, mono=True) # 22050, v clanku sample rate 16000
    y = y / max(abs(y)) * 0.9

    fftsize = 2048  # <-- google clanek # 1024  # google 512   # 2048 # cca 100ms
    stepsize = 512  # <-- google clanek # 512 # google 256   # zamaknjenost - lahko isto k fftsize sam ne bo prekrivanja
    D = librosa.feature.melspectrogram(y, 16000 ,n_fft=fftsize,hop_length=stepsize,n_mels=229, fmin=50, fmax=8000) # 22050 sample rate, v clanku 16000, n_mels=64, v clanku 229
    D = np.log(D + 1e-8).transpose()# logaritem(se prav doMel=1, 3, 4 al neki) velikosti oken, ipd, sample rate zna bit tud drugacen
    freqs=librosa.mel_frequencies(n_mels=229, fmin=50, fmax=8000) # n_mels=64, v clanku 229 logarithmically-spaced frequency bins
    return D,freqs,sr/stepsize

def generate_features():
    folders = ['audioData']
    fl = dataset_files_labels(folders)

    for (fileName, label) in fl:
        # print(fileName)
        # print(label)
        features,freqs,sr=calc_features(fileName)

        write_features(features, label, fileName + '.mel.tfr')


def main(_):
    generate_features()


if __name__ == '__main__':
    tf.app.run()

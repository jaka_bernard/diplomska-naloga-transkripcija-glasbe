from libs.activations import lrelu
from libs.model_utils import conv2d, linear, batch_normalize
import os.path
import tensorflow as tf
from tensorflow.python.framework import importer
from tensorflow.python.framework import ops

def accuracy(predictions, labels):
    # Return the number of true entries.
    # correct_prediction = tf.equal(tf.argmax(y_pred, 1), tf.argmax(trainlabels, 1))
    # accuracy = tf.reduce_mean(tf.cast(correct_prediction, 'float'))

    #correct_prediction = tf.equal(tf.argmax(predictions, 1), labels)
    #return tf.reduce_sum(tf.cast(correct_prediction, tf.int32), name='correct_sum')

    correct_prediction = tf.nn.in_top_k(predictions, labels, 1) # handles ties better
    return tf.reduce_sum(tf.cast(correct_prediction, tf.int32),name='correct_sum')


def loss(predictions, labels):
    # %% Define loss/eval/training functions
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=predictions, labels=labels, name='xentropy')
    meanentropy = tf.reduce_mean(cross_entropy, name='xentropy_mean')
    # loss = -tf.reduce_sum(trainlabels * tf.log(y_pred))

    return meanentropy


def build_onelayer(data, output_size, training=True):
    x_tensor = tf.expand_dims(data, -1)

    with tf.variable_scope('conv1'):
        w = tf.get_variable(
            'w', [3, 3, x_tensor.get_shape()[-1], 12],
            initializer=tf.truncated_normal_initializer(stddev=0.02))

        conv = tf.nn.conv2d(x_tensor, w, strides=[1, 1, 1, 1], padding='SAME')

        conv = batch_normalize(conv)

        sz = conv.get_shape().as_list()
        conv = tf.reshape(conv, [-1, sz[1] * sz[2] * sz[3]])

        conv = linear(conv, output_size, bias=True, activation=lambda x: tf.nn.softmax(x), name='out1')

        return conv


def build_model(data, output_size, is_training):
    x_tensor = tf.expand_dims(data, -1)
    return kaiming_residual(x_tensor, output_size, is_training)


def kaiming_plain(inp, output_size):
    nfilt = 16

    net = inp
    # net = conv2d(net, nfilt, 6, 6, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv1')
    # net = tf.nn.max_pool(net, [1,3,3,1], [1,2,2,1], padding='SAME')

    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv21')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv22')

    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv23')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv24')

    nfilt = nfilt * 2
    net = conv2d(net, nfilt, 3, 3, 2, 2, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv31')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv32')

    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv33')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv34')

    nfilt = nfilt * 2
    net = conv2d(net, nfilt, 3, 3, 2, 2, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv41')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv42')

    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv43')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv44')

    #    nfilt=nfilt*2
    #    net = conv2d(net, nfilt, 3, 3, 2, 2, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv51')
    #    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv52')

    #    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv53')
    #    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv54')

    ns = net.get_shape().as_list()
    net = tf.nn.avg_pool(net, [1, ns[1], ns[2], 1], [1, ns[1], ns[2], 1], padding='SAME')

    ns = net.get_shape().as_list()
    net = tf.reshape(net, [-1, ns[1] * ns[2] * ns[3]])
    # net = linear(net, 200, bias=True, activation=lambda x: tf.nn.relu(x), name='full1')
    # if training:
    #    net = tf.nn.dropout(net, 0.5)
    # else:
    #    net = tf.nn.dropout(net, 1.0)

    net = linear(net, output_size, bias=True, activation=lambda x: tf.nn.softmax(x), name='out')

    return net


def kaiming_residual(data, output_size, is_training, n_filt=8, n_res_lay=3, layer1_size=8, use_max_pool=False, use_avg_pool=True):

    inp = tf.expand_dims(data, -1)

    nfilt = n_filt

    use_batch_norm = True

    net = inp

    # 8x8 max avg 92-94 92.6 with 8 filters, 92.6 with 16 filters
    # 8x4 avg max 90 with 8 filters
    net = conv2d(net, nfilt, layer1_size, layer1_size, 1, 1, batch_norm=use_batch_norm, is_training=is_training, bias=False, activation=lambda x: tf.nn.relu(x), name='conv0')

    if use_max_pool:
        net = tf.nn.max_pool(net, [1, 3, 3, 1], [1, 2, 2, 1], padding='SAME')
    if use_avg_pool:
        net = tf.nn.avg_pool(net, [1, 3, 1, 1], [1, 3, 1, 1], padding='SAME')

    # 93, 92 with 8
    #net = conv2d(net, nfilt, 6, 6, 1, 1, batch_norm=use_batch_norm, bias=False, activation=lambda x: tf.nn.relu(x),
    #             name='convMel')
    #net = tf.nn.max_pool(net, [1, 3, 3, 1], [1, 2, 2, 1], padding='SAME')
    #net = conv2d(net, nfilt, 6, 3, 1, 1, batch_norm=use_batch_norm, bias=False, activation=lambda x: tf.nn.relu(x),
    #             name='convMel1')
    #net = tf.nn.max_pool(net, [1, 3, 1, 1], [1, 3, 1, 1], padding='SAME')

    # net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=use_batch_norm, bias=False, activation=lambda x: tf.nn.relu(x), name='convMel')
    # net = tf.nn.max_pool(net, [1, 4, 2, 1], [1, 4, 2, 1], padding='SAME')
    # net = conv2d(net, nfilt, 6, 3, 1, 1, batch_norm=use_batch_norm, bias=False, activation=lambda x: tf.nn.relu(x), name='convMel1')
    # net = tf.nn.max_pool(net, [1, 3, 1, 1], [1, 3, 1, 1], padding='SAME')

    # 91.8
    # net = conv2d(net, nfilt, 6, 2, 1, 1, batch_norm=use_batch_norm, bias=False, activation=lambda x: tf.nn.relu(x), name='convMel')
    # net = tf.nn.max_pool(net, [1, 3, 1, 1], [1, 2, 1, 1], padding='SAME')
    # net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=use_batch_norm, bias=False, activation=lambda x: tf.nn.relu(x), name='convMel1')
    # net = tf.nn.max_pool(net, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')

    #resnet building block
    for i in range(1,n_res_lay):
        net = resnet_block(net, nfilt, use_batch_norm, is_training, 'res' + str(i) + '1')
        net = resnet_block(net, nfilt, use_batch_norm, is_training, 'res' + str(i) + '2')
        nfilt *=2
    net = resnet_block(net, nfilt, True, is_training, 'res' + str(i+1) + '1')
    net = resnet_block(net, nfilt, True, is_training, 'res' + str(i+1) + '2')


    #ns = net.get_shape().as_list()
    #net = tf.nn.avg_pool(net, [1, ns[1], ns[2], 1], [1, ns[1], ns[2], 1], padding='SAME')

    # 1x1 convolution
    net = conv2d(net, 1, 1, 1, 1, 1, batch_norm=False, is_training=is_training, bias=False, activation=lambda x: tf.nn.relu(x), name='gather')

    ns = net.get_shape().as_list()
    net = tf.reshape(net, [-1, ns[1] * ns[2] * ns[3]])
    # net = linear(net, 200, bias=True, activation=lambda x: tf.nn.relu(x), name='full1')
    # if training:
    #    net = tf.nn.dropout(net, 0.5)
    # else:
    #    net = tf.nn.dropout(net, 1.0)

    #net = linear(net, output_size, bias=True, activation=lambda x: tf.nn.softmax(x), name='out')
    net = linear(net, output_size, bias=True, activation=lambda x: x, name='out')

    return net


def resnet_block(net, nfilt, use_batch_norm, is_training, name):
    if (net.shape[3]==nfilt):
        netp = net
        net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=False, is_training=is_training, bias=False, activation=lambda x: tf.nn.relu(x), name=name + 'a')
        net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=use_batch_norm, is_training=is_training, bias=False, activation=lambda x: x, name=name + 'b')
    elif (net.shape[3]==nfilt/2):
        netp = tf.nn.avg_pool(
            conv2d(net, nfilt, 1, 1, 1, 1, batch_norm=use_batch_norm, is_training=is_training, bias=False, activation=lambda x: x, name=name + '/2'),
            [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        # net = tf.nn.max_pool(conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=use_batch_norm, bias=False, activation=lambda x: tf.nn.relu(x), name='conv31'),[1,2,2,1],[1,2,2,1],padding='SAME')
        net = conv2d(net, nfilt, 3, 3, 2, 2, batch_norm=False, is_training=is_training, bias=False, activation=lambda x: tf.nn.relu(x), name=name + 'a')
        net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=use_batch_norm, is_training=is_training, bias=False, activation=lambda x: x, name=name + 'b')
    return tf.nn.relu(tf.add(net, netp))


def kaiming_residual_bottleneck(inp, output_size):
    nfilt = 16

    net = inp
    # net = conv2d(net, nfilt, 6, 6, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv1')
    # net = tf.nn.max_pool(net, [1,3,3,1], [1,2,2,1], padding='SAME')

    netp = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                  name='conv21x')
    net = conv2d(net, nfilt, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv21')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv22')
    net = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                 name='conv23')
    net = tf.nn.relu(tf.add(net, netp))

    netp = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                  name='conv24x')
    net = conv2d(net, nfilt, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv24')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv25')
    net = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                 name='conv26')
    net = tf.nn.relu(tf.add(net, netp))

    nfilt = nfilt * 2
    netp = tf.nn.avg_pool(
        conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
               name='conv31x'), [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
    net = tf.nn.avg_pool(conv2d(net, nfilt, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                                name='conv31'), [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv32')
    net = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                 name='conv33')
    net = tf.nn.relu(tf.add(net, netp))

    netp = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                  name='conv34x')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv35')
    net = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                 name='conv36')
    net = tf.nn.relu(tf.add(net, netp))

    nfilt = nfilt * 2
    netp = tf.nn.avg_pool(
        conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
               name='conv41x'), [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
    net = tf.nn.avg_pool(conv2d(net, nfilt, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                                name='conv41'), [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv42')
    net = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                 name='conv43')
    net = tf.nn.relu(tf.add(net, netp))

    netp = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                  name='conv44x')
    net = conv2d(net, nfilt, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv44')
    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x),
                 name='conv45')
    net = conv2d(net, nfilt * 4, 1, 1, 1, 1, batch_norm=True, bias=False, activation=lambda x: x,
                 name='conv46')
    net = tf.nn.relu(tf.add(net, netp))

    #    nfilt=nfilt*2
    #    net = conv2d(net, nfilt, 3, 3, 2, 2, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv51')
    #    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv52')

    #    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv53')
    #    net = conv2d(net, nfilt, 3, 3, 1, 1, batch_norm=True, bias=False, activation=lambda x: tf.nn.relu(x), name='conv54')

    ns = net.get_shape().as_list()
    net = tf.nn.avg_pool(net, [1, ns[1], ns[2], 1], [1, ns[1], ns[2], 1], padding='SAME')

    ns = net.get_shape().as_list()
    net = tf.reshape(net, [-1, ns[1] * ns[2] * ns[3]])
    # net = linear(net, 200, bias=True, activation=lambda x: tf.nn.relu(x), name='full1')
    # if training:
    #    net = tf.nn.dropout(net, 0.5)
    # else:
    #    net = tf.nn.dropout(net, 1.0)

    net = linear(net, output_size, bias=True, activation=lambda x: tf.nn.softmax(x), name='out')

    return net


def mlp(data, output_size, layer2_size=16):
    net = tf.expand_dims(data, -1)
    ns = net.get_shape().as_list()
    net = tf.reshape(net, [-1, ns[1] * ns[2] * ns[3]])
    if (layer2_size>0):
        net = linear(net, layer2_size, bias=True, activation=lambda x: tf.nn.tanh(x), name='l2')
    net = linear(net, output_size, bias=True, activation=lambda x: x, name='out')

    return net


def savemodel(folder, step, sess, summary_op, summary_writer, saver, isfinal):
    if step % 10 == 0 or isfinal:
        summary_str = sess.run(summary_op)
        summary_writer.add_summary(summary_str, step)

        # Save the model checkpoint periodically.
    model = False
    if step % 20 == 0 or isfinal:
        checkpoint_path = os.path.join(folder, 'model.ckpt')
        saver.save(sess, checkpoint_path, global_step=step)
        with sess.graph.as_default():
            tf.train.write_graph(sess.graph_def, folder, 'model.ckpt.writegraf', as_text=True)
            tf.train.export_meta_graph(os.path.join(folder, 'model.ckpt.exportmetagraph'),
                                       collection_list=['trainable_variables','variables'],
                                       saver_def=saver.saver_def, graph_def=sess.graph_def)
        model = True

    return model


def import_meta_graph(file, input_map = None):
  """Recreates a Graph saved in a a `MetaGraphDef` proto.

  This function adds all the nodes from the meta graph def proto to the current
  graph, recreates all the collections, and returns a saver from saver_def.

  Args:
    meta_graph_def: `MetaGraphDef` protocol buffer.

  Returns:
    A saver constructed rom `saver_def` in `meta_graph_def`.
  """
  meta_graph_def= tf.train.import_meta_graph(file)

  # Gathers the list of nodes we are interested in.
  importer.import_graph_def(meta_graph_def.graph_def, name="", input_map=input_map)

  # Restores all the other collections.
  for key, col_def in meta_graph_def.collection_def.items():
    kind = col_def.WhichOneof("kind")
    if kind is None:
      #logging.error("Cannot identify data type for collection %s. Skipping." % key)
      continue
    from_proto = ops.get_from_proto_function(key)
    if from_proto:
      assert kind == "bytes_list"
      proto_type = ops.get_collection_proto_type(key)
      for value in col_def.bytes_list.value:
        proto = proto_type()
        proto.ParseFromString(value)
        ops.add_to_collection(key, from_proto(proto))
    else:
      field = getattr(col_def, kind)
      if kind == "node_list":
        for value in field.value:
          col_op = ops.get_default_graph().as_graph_element(value)
          ops.add_to_collection(key, col_op)
      elif kind == "int64_list":
        # NOTE(opensource): This force conversion is to work around the fact
        # that Python2 distinguishes between int and long, while Python3 has
        # only int.
        for value in field.value:
          ops.add_to_collection(key, int(value))
      else:
        for value in field.value:
          ops.add_to_collection(key, value)

  if meta_graph_def.HasField("saver_def"):
    return tf.train.Saver(saver_def=meta_graph_def.saver_def)
  else:
    return tf.train.Saver()
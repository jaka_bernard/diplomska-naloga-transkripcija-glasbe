from __future__ import absolute_import
from __future__ import division

from datetime import datetime

import read_data as tts
import numpy as np
from model import *
import sklearn
import tensorflow.contrib.slim   as slim

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string('eval_dir', 'eval_model', """Directory where to write event logs.""")
tf.app.flags.DEFINE_string('checkpoint_dir', '/output/trained_model', """Directory where to read model checkpoints.""")


def test_model(sess, iterator_test, next_values_test, num_categories, ncorrect, predidx):
    true_count = 0  # Counts the number of correct predictions.
    nread = 0.0
    cm = np.zeros((num_categories, num_categories), dtype=np.int64)

    #ncorrect = sess.graph.as_graph_element("correct_sum:0")
    #predidx = sess.graph.as_graph_element("label_idx:1")

    sess.run(iterator_test.initializer)

    while True:
        try:
            values_test = sess.run(next_values_test)
            ncorrect_value, pi = sess.run([ncorrect, predidx], feed_dict={'x_input:0': values_test[0], 'y_output:0': values_test[1]})
            true_count += ncorrect_value
            nread += 1
            cm = cm = cm + sklearn.metrics.confusion_matrix(values_test[1], pi, range(0, num_categories))
        except tf.errors.OutOfRangeError:
            print('Done testing.')
            break
    print(cm)
    precision = true_count / nread
    print('Precision @ 1 = %.3f' % (precision))
    return precision


def test_model_slim(sess, iterator_test, test_cm, test_predidx, test_ncorrect):
    true_count = 0  # Counts the number of correct predictions.
    nread = 0.0

    #ncorrect = sess.graph.as_graph_element("correct_sum:0")
    #predidx = sess.graph.as_graph_element("label_idx:1")

    cm=[]
    sess.run(iterator_test.initializer)

    while True:
        try:
            conf_mat, predidx, ncorrect = sess.run([test_cm, test_predidx, test_ncorrect])
            true_count += ncorrect
            nread += len(predidx)
            if (len(cm)==0):
                cm=conf_mat
            else:
                cm = cm + conf_mat
        except tf.errors.OutOfRangeError:
            print('Done testing.')
            break
    print(cm)
    precision = true_count / nread
    print('Precision @ 1 = %.3f' % (precision))
    return precision

def evaluateWithLoad(testset, allLabels, run_once=True):
    with tf.Graph().as_default():

        num_categories = len(allLabels)
        data, labels = tts.input_pipeline(testset, allLabels, FLAGS.test_batch_size, 2, False)

        saver = tf.train.import_meta_graph(os.path.join(FLAGS.checkpoint_dir, 'model.ckpt.exportmetagraph'), input_map={'inputs:0': data})
        #saver = import_meta_graph(os.path.join(FLAGS.checkpoint_dir, 'model.ckpt.exportmetagraph'), input_map={'inputs:0': data})

        #saver = tf.train.Saver(tf.trainable_variables())
        with tf.Session() as sess:
            predictions = sess.graph.as_graph_element("out/Softmax:0")
            doTest = sess.graph.as_graph_element("dotest:0")

            # predictions = build_model(data, num_categories, training=False)
            ncorrect = accuracy(predictions, labels)
            _, predidx = tf.nn.top_k(predictions)

            ckpt = tf.train.get_checkpoint_state(FLAGS.checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                sess.run(tf.initialize_all_variables())
                saver.restore(sess, ckpt.model_checkpoint_path)
            else:
                print('No checkpoint file found')
                return



            #saver = tf.train.Saver(tf.trainable_variables())

            # Build the summary operation based on the TF collection of Summaries.
            #summary_op = tf.merge_all_summaries()

            #graph_def = sess.graph.as_graph_def(add_shapes=True)
            #summary_writer = tf.train.SummaryWriter(FLAGS.eval_dir, graph_def=graph_def)

            # Start the queue runners.
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                # threads = []
                # for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
                #    threads.extend(qr.create_threads(sess, coord=coord, daemon=True, start=True))

                true_count = 0  # Counts the number of correct predictions.
                step = 0
                nread = 0.0
                cm = np.zeros((num_categories, num_categories), dtype=np.int64)
                while not coord.should_stop() and nread < len(testset):
                    nc, pi, lb = sess.run([ncorrect, predidx, labels], feed_dict={doTest: 1.0})
                    #preds = sess.run([predictions])
                    true_count += nc
                    nread += FLAGS.test_batch_size
                    #print('%d %d %d %d' % (step, predictions[0], true_count, nread))
                    cm = cm + sklearn.metrics.confusion_matrix(lb, pi, range(0, num_categories))
                    step += 1

                # Compute precision @ 1.
                precision = true_count / nread
                print('%s: precision @ 1 = %.3f' % (datetime.now(), precision))
                #print('Confusion matrix')
                print(cm)

                #summary = tf.Summary()
                #summary.ParseFromString(sess.run(summary_op))
                #summary.value.add(tag='Precision @ 1', simple_value=precision)
                #summary_writer.add_summary(summary)
            except tf.errors.OutOfRangeError:
                print('Done testing in %d steps.' % (step))
            #except Exception as e:  # pylint: disable=broad-except
            #    coord.request_stop(e)
            finally:
                # When done, ask the threads to stop.
                coord.request_stop()

            coord.join(threads)

def evaluateWithBuild(testset, allLabels, run_once=True):
    with tf.Graph().as_default():

        doTest = tf.placeholder(tf.float32, [], name="dotest")
        numSteps = tf.Variable(tf.zeros([], dtype=tf.int32), name="numsteps", trainable=False)

        num_categories = len(allLabels)
        data, labels = tts.input_pipeline(testset, allLabels, FLAGS.test_batch_size, 2, False)

        predictions = build_model(data, num_categories, training=False)
        ncorrect = accuracy(predictions, labels)
        _, predidx = tf.nn.top_k(predictions)

        saver = tf.train.Saver(tf.trainable_variables())

        with tf.Session() as sess:

            ckpt = tf.train.get_checkpoint_state(FLAGS.checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                sess.run(tf.initialize_all_variables())
                saver.restore(sess, ckpt.model_checkpoint_path)
            else:
                print('No checkpoint file found')
                return

            # Build the summary operation based on the TF collection of Summaries.
            #summary_op = tf.merge_all_summaries()

            #graph_def = sess.graph.as_graph_def(add_shapes=True)
            #summary_writer = tf.train.SummaryWriter(FLAGS.eval_dir, graph_def=graph_def)

            # Start the queue runners.
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                # threads = []
                # for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
                #    threads.extend(qr.create_threads(sess, coord=coord, daemon=True, start=True))

                true_count = 0  # Counts the number of correct predictions.
                step = 0
                nread = 0
                cm = np.zeros((num_categories, num_categories), dtype=np.int64)
                while not coord.should_stop() and nread < len(testset):
                    nc, pi, lb = sess.run([ncorrect, predidx, labels], feed_dict={doTest: 1.0})
                    #preds = sess.run([predictions])
                    true_count += nc
                    nread += FLAGS.test_batch_size
                    #print('%d %d %d %d' % (step, predictions[0], true_count, nread))
                    cm = cm + sklearn.metrics.confusion_matrix(lb, pi, range(0, num_categories))
                    step += 1

                # Compute precision @ 1.
                precision = true_count / nread
                print('%s: precision @ 1 = %.3f' % (datetime.now(), precision))
                #print('Confusion matrix')
                print(cm)

                #summary = tf.Summary()
                #summary.ParseFromString(sess.run(summary_op))
                #summary.value.add(tag='Precision @ 1', simple_value=precision)
                #summary_writer.add_summary(summary)
            except tf.errors.OutOfRangeError:
                print('Done testing in %d steps.' % (step))
            #except Exception as e:  # pylint: disable=broad-except
            #    coord.request_stop(e)
            finally:
                # When done, ask the threads to stop.
                coord.request_stop()

            coord.join(threads)

def main(_):
    try:
        if tf.gfile.Exists(FLAGS.eval_dir):
            tf.gfile.DeleteRecursively(FLAGS.eval_dir)
        tf.gfile.MakeDirs(FLAGS.eval_dir)
    except Exception as e:
        i=0

    _, testset, allLabels = tts.train_test_set(['1', '2', 'i', 'b', 's'], 0.33, ['samples'], None, do_mel=0)
    evaluateWithLoad(testset, allLabels, True)


if __name__ == '__main__':
    tf.app.run()

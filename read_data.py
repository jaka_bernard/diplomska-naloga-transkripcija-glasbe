import csv
from sklearn.model_selection import train_test_split
import numpy as np
from globals import *
import os
from tensorflow.contrib.signal.python.ops import shape_ops

def dataset_files_labels(folders):
    """Read all samples and labels from files, return a list of all filename/label pairs."""
    fl = []
    root = FLAGS.DBRoot
    root = './data'
    for f in folders:

        fo = open(root + '/sample labels ' + f + '.txt', 'r')
        dialect = csv.Sniffer().sniff(fo.read(1024), delimiters="\t ")
        fo.seek(0)
        for x in csv.reader(fo, dialect):
            fl.append([root + '/' + f + '/' + x[0], x[1]])
    return fl


def train_test_set(types, testpercent, folders=['effects', 'gtzan', 'mirex2015', 'gni'], size=None):
    """
    Creates a list of files for the train and test set
    :param types: combination of '1' '2' 'b' 'e' 'h' 'i' 'l' 'm' 's' 'u' 'w' 'x'
    :param testpercent: percentage of test cases to produce
    :param folders: optional, list can include one of ['effects', 'gtzan', 'mirex2015', 'samples']
    :param size: optional, size of entire (train+test) set, or None if all should be used
    :return: train and test  lists containing tuples [folder, label], and a list of all labels
    """
    fl = dataset_files_labels(folders)

    fl = [(elem[0] + '.mel.tfr', elem[1]) for elem in fl if elem[1] in types]
    FLAGS.num_freqs=229


    if size is None:
        size = len(fl)

    trainsize = int(round(size * (1 - testpercent)))
    testsize = size - trainsize

    if (trainsize==0):
        train=[]
        test=fl
    else:
        train, test = train_test_split(fl,
                                   train_size=trainsize,
                                   test_size=testsize,
                                   stratify=[x[1] for x in fl],
                                   random_state=42)

    all_labels=list(sorted(set([x[1] for x in fl])))
    return train, test, all_labels


def dataset_map_tfr_single(tfrecord):
    """ Takes a single random sample out of each tfr"""
    context_out, feat_list_out = tf.parse_single_sequence_example(
        tfrecord,
        context_features={
            "label": tf.FixedLenFeature((1,), dtype=tf.string)
        },
        sequence_features={
            "spectrogram": tf.FixedLenSequenceFeature((FLAGS.num_freqs,), tf.float32),
        }
    )
    spectrogram_all = feat_list_out['spectrogram']
    shp = tf.shape(spectrogram_all)
    frame_offset = tf.random_uniform([1],minval=0,maxval=tf.subtract(shp[0],FLAGS.num_timesteps),dtype=tf.int32,seed=42) # izberes random frame
    slice_start = tf.concat([frame_offset,[0]],0)

    slice = tf.slice(spectrogram_all, slice_start, [FLAGS.num_timesteps, FLAGS.num_freqs]) #spektrogram razreze na frame
    spectrogram_raw = tf.expand_dims(tf.transpose(slice), -1)


    lbl = context_out['label']

    _, idx = tf.setdiff1d(tf.constant(FLAGS.all_labels), lbl)
    idx, _ = tf.setdiff1d(tf.range(len(FLAGS.all_labels)), idx)

    return spectrogram_raw, idx[0] # vrnes na random 2 frama in vrnes zraven index labela v all_labels --> to sta x in y!


def dataset_map_tfr_multi(tfrecord):
    """ Takes consequtive samples with step size out of each tfr"""
    context_out, feat_list_out = tf.parse_single_sequence_example(
        tfrecord,
        context_features={
            "label": tf.FixedLenFeature((1,), dtype=tf.string)
        },
        sequence_features={
            "spectrogram": tf.FixedLenSequenceFeature((FLAGS.num_freqs,), tf.float32),
        }
    )
    spectrogram_all = feat_list_out['spectrogram']

    dim3=1
    spectrogram_raw = tf.expand_dims(spectrogram_all, -1)

    spectrogram_frames = tf.transpose(shape_ops.frame(spectrogram_raw, FLAGS.num_timesteps, FLAGS.test_step_size, axis=0),[0,2,1,3]) # cel file se razbije v num_timesteps dolge koščke

    lbl = context_out['label']

    _, idx = tf.setdiff1d(tf.constant(FLAGS.all_labels), lbl)
    idx, _ = tf.setdiff1d(tf.range(len(FLAGS.all_labels)), idx)

    lblIndex = tf.fill([tf.shape(spectrogram_frames)[0]], tf.to_int64(idx[0])) # index za 1 file samo 1, na outputu toliko indexov kot je frame-ov, s fillom jih razmnozis
    #files = tf.fill([tf.shape(spectrogram_frames)[0]], tfrecord)
    return spectrogram_frames, lblIndex #, files                 spet x in y, s tem da ni samo 1 example ampak kolikor je file dolg

def write_features(D, label, file_name):
    '''
    writes features to tensorflow serialized file
    :param D: matrix of features. time dimension should be the first
    :param label: label to write with features
    :param file_name: file name to write in
    '''
    # TODO: spremeni v korektne labele
    # TODO: 2D lables bcuz why not

    print('Writing', file_name)
    writer = tf.python_io.TFRecordWriter(file_name)

    if D.ndim==2:
        example = tf.train.SequenceExample(
            context = tf.train.Features(feature = {
                "label": tf.train.Feature(bytes_list=tf.train.BytesList(value=[label.encode()]))
                }),
            feature_lists=tf.train.FeatureLists(
                feature_list={
                "spectrogram":
                    tf.train.FeatureList(
                        feature=[
                            tf.train.Feature(
                                float_list=tf.train.FloatList(
                                    value=d))
                            for d in D
                        ]
                    ),
                }
            )
        )
    else:
        example = tf.train.SequenceExample(
            context = tf.train.Features(feature = {
                "label": tf.train.Feature(bytes_list=tf.train.BytesList(value=[label.encode()]))
                }),
            feature_lists=tf.train.FeatureLists(
                feature_list={
                "spectrogram":
                    tf.train.FeatureList(
                        feature=[
                            tf.train.Feature(
                                float_list=tf.train.FloatList(
                                    value=np.reshape(d,[d.shape[0]*d.shape[1]])))
                            for d in D
                        ]
                    ),
                }
            )
        )


    writer.write(example.SerializeToString())
    writer.close()
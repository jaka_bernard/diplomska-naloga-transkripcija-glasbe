import tensorflow as tf
from tensorflow.python.framework import ops
import tensorflow.contrib.slim as slim


def accuracy(predictions, labels):
    # Return the number of true entries.
    # correct_prediction = tf.equal(tf.argmax(y_pred, 1), tf.argmax(trainlabels, 1))
    # accuracy = tf.reduce_mean(tf.cast(correct_prediction, 'float'))

    #correct_prediction = tf.equal(tf.argmax(predictions, 1), labels)
    #return tf.reduce_sum(tf.cast(correct_prediction, tf.int32), name='correct_sum')

    correct_prediction = tf.nn.in_top_k(predictions, labels, 1) # handles ties better
    return tf.reduce_sum(tf.cast(correct_prediction, tf.int32),name='correct_sum')

def kaiming_residual(inputs, num_classes, reuse = None, is_training=True, n_filt=8, n_res_lay=3, layer1_size=8, use_max_pool=False, use_avg_pool=True, use_atrous=True, n_modules=1, scope=''):

    with slim.variable_scope.variable_scope(scope, 'Model', [inputs, num_classes], reuse=reuse):
        with slim.arg_scope([slim.batch_norm, slim.dropout], is_training=is_training):
            with slim.arg_scope([slim.conv2d, slim.max_pool2d, slim.avg_pool2d], stride=1, padding='SAME'):

                net = slim.conv2d(inputs, n_filt, [layer1_size, layer1_size], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv00')
                if use_atrous:
                    net = tf.concat([net, slim.conv2d(inputs, n_filt, [layer1_size, layer1_size],rate = 2, normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv01')], axis=3)
                #net = tf.concat([net, slim.conv2d(inputs, n_filt, [layer1_size, layer1_size],rate = 3, normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv02')], axis=3)
                #net = tf.concat([net, slim.conv2d(inputs, 4, [24, 3], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv02')], axis=3)
                #net = tf.concat([net, slim.conv2d(inputs, 4, [3, 24], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv03')], axis=3)

                n_filt = net.shape[3].value

                if use_max_pool:
                    net = slim.max_pool2d(net,[3,3],stride=2,scope='pool1')
                if use_avg_pool:
                    net = slim.max_pool2d(net, [3, 1], stride=[3,1],scope='pool2')

                for i in range(1,n_res_lay+1):
                    for j in range(0,n_modules):
                        net = resnet_block(net, n_filt, 'res' + str(i) + str(j))
                    n_filt *=2


                net = slim.batch_norm(net, scope='postnorm')

                # 1x1 convolution
                net = slim.conv2d(net, 1, [1, 1], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='gather')

                net = slim.flatten(net, scope='flatten')

                logits = slim.fully_connected(net, num_classes, normalizer_fn=None, activation_fn=None, scope='logits') # restore=restore_logits

                predictions = slim.nn_ops.softmax(logits, name='predictions')

    return logits, predictions


def regular(inputs, num_classes, reuse = None, is_training=True, n_filt=8, n_lay=3, layer1_size=8, use_max_pool=False, use_avg_pool=True, use_atrous=True, n_modules=1, scope=''):

    with slim.variable_scope.variable_scope(scope, 'Model', [inputs, num_classes], reuse=reuse):
        with slim.arg_scope([slim.batch_norm, slim.dropout], is_training=is_training):
            with slim.arg_scope([slim.conv2d, slim.max_pool2d, slim.avg_pool2d], stride=1, padding='SAME'):

                net = slim.conv2d(inputs, n_filt, [layer1_size, layer1_size], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv00')
                if use_atrous:
                    net = tf.concat([net, slim.conv2d(inputs, n_filt, [layer1_size, layer1_size], rate=2, normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv01')], axis=3)
                # net = tf.concat([net, slim.conv2d(inputs, n_filt, [layer1_size, layer1_size],rate = 3, normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv02')], axis=3)
                # net = tf.concat([net, slim.conv2d(inputs, 4, [24, 3], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv02')], axis=3)
                # net = tf.concat([net, slim.conv2d(inputs, 4, [3, 24], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='conv03')], axis=3)

                n_filt = net.shape[3].value

                if use_max_pool:
                    net = slim.max_pool2d(net, [3, 3], stride=2, scope='pool1')
                if use_avg_pool:
                    net = slim.max_pool2d(net, [3, 1], stride=[3, 1], scope='pool2')

                for i in range(1,n_lay+1):
                    for j in range(0,n_modules):
                        net = regular_block(net, n_filt, [3, 3], 'reg' + str(i) + '1')
                    n_filt *=2

                net = slim.batch_norm(net, scope='postnorm')

                # 1x1 convolution
                net = slim.conv2d(net, 1, [1, 1], normalizer_fn=None, activation_fn=None, biases_initializer=None, scope='gather')

                #net = ops.dropout(net, dropout_keep_prob, scope='dropout')
                net = slim.flatten(net, scope='flatten')

                logits = slim.fully_connected(net, num_classes, normalizer_fn=None, activation_fn=None, scope='logits') # restore=restore_logits

                predictions = slim.nn_ops.softmax(logits, name='predictions')

    return logits, predictions


def kaiming_residual_parametes(weight_decay=0.00004, batch_norm_decay=0.9997, batch_norm_epsilon=0.001, reg=3, elu=True):
  """Yields the scope with the default parameters for kaiming_residual.
  Args:
    weight_decay: The weight decay to use for regularizing the model.
    batch_norm_decay: Decay for batch norm moving average
    batch_norm_epsilon: Small float added to variance to avoid division by zero
    use_fused_batchnorm: Enable fused batchnorm.
  Returns:
    An `arg_scope` to use for the inception v3 model.
  """
  batch_norm_params = {
      # Decay for the moving averages.
      'decay': batch_norm_decay,
      # epsilon to prevent 0s in variance.
      'epsilon': batch_norm_epsilon,
      # collection containing update_ops.
      #'updates_collections': slim.ops.GraphKeys.UPDATE_OPS,
      # Use fused batch norm if possible.
      #'fused': True,
      # collection containing the moving mean and moving variance.
      'variables_collections': {
          'beta': None,
          'gamma': None,
          'moving_mean': ['moving_vars'],
          'moving_variance': ['moving_vars'],
      }
  }
  wr=None
  if reg==1:
      wr=slim.regularizers.l1_regularizer(weight_decay)
  elif reg==2:
      wr = slim.regularizers.l2_regularizer(weight_decay)
  elif reg==3:
      wr = slim.regularizers.l1_l2_regularizer(weight_decay, weight_decay)
  with slim.arg_scope(
          [slim.conv2d, slim.fully_connected],
          weights_regularizer=wr):
    with slim.arg_scope(
        [slim.conv2d, slim.batch_norm],
        activation_fn=slim.nn_ops.elu if elu is True else slim.nn_ops.relu):
      with slim.arg_scope(
          [slim.layers.conv2d],
          weights_initializer=slim.initializers.variance_scaling_initializer(),
          normalizer_fn=slim.batch_norm,
          normalizer_params=batch_norm_params
            ) as sc:
        return sc


def regular_block(inputs, n_filt, filtsize = [3, 3], scope = ''):
    with tf.variable_scope(scope):

        preact = slim.batch_norm(inputs, scope='preact')

        if inputs.shape[3]==n_filt:
            stride=1
        elif inputs.shape[3] == n_filt / 2:
            stride=2

        net = slim.conv2d(preact, n_filt, filtsize, stride=stride)
        output = slim.conv2d(net, n_filt, filtsize, stride=stride, normalizer_fn=None, activation_fn=None, biases_initializer=None)
        return output

def resnet_block(inputs, n_filt, scope):
    with tf.variable_scope(scope):

        preact = slim.batch_norm(inputs, scope='preact')

        if inputs.shape[3]==n_filt:
                shortcut = inputs
                stride=1
        elif inputs.shape[3] == n_filt / 2:
            stride=2
            shortcut = slim.conv2d(preact, n_filt, [1, 1], stride=stride, normalizer_fn=None, activation_fn=None, biases_initializer = None)

        residual = slim.conv2d(preact, n_filt, [3, 3], stride=stride)
        residual = slim.conv2d(residual, n_filt, [3, 3], normalizer_fn=None, activation_fn=None, biases_initializer=None)

        output = shortcut + residual

        return output


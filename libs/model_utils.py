"""Useful connections for TensorFlow.
Parag K. Mital, Jan 2016.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf


def batch_normalize(tensor_in, is_training, epsilon=1e-3, convnet=True, decay=0.95, name='batch_norm'):
    """Batch Normalization

    Args:
        tensor_in: input Tensor, 4D shape:
                   [batch, in_height, in_width, in_depth].
        epsilon : A float number to avoid being divided by 0.
        decay: decay rate for exponential moving average.
        convnet: Whether this is for convolutional net use. If this is True,
                 moments will sum across axis [0, 1, 2]. Otherwise, only [0].
        scale_after_normalization: Whether to scale after normalization.
        training: if used during training, update ewma, otherwise just take mean and variance
        name: name to use for scope
    """
    shape = tensor_in.get_shape().as_list()

    with tf.variable_scope(name):

        mean = tf.get_variable("mean", [shape[-1]], initializer=tf.constant_initializer(0.), trainable=False)
        variance = tf.get_variable("variance", [shape[-1]], initializer=tf.constant_initializer(1.), trainable=False)
        gamma = tf.get_variable("gamma", [shape[-1]], initializer=tf.random_normal_initializer(1., 0.02))
        beta = tf.get_variable("beta", [shape[-1]], initializer=tf.constant_initializer(0.))

        numsteps = tf.get_default_graph().as_graph_element("numsteps:0")

        ewma = tf.train.ExponentialMovingAverage(decay=decay,num_updates = numsteps,name='ewma')
        if convnet:
            m, v = tf.nn.moments(tensor_in, [0, 1, 2],name='moments')
        else:
            m, v = tf.nn.moments(tensor_in, [0],name='moments')

        emap = ewma.apply([m, v])
        ewa_mean, ewa_var = ewma.average(m), ewma.average(v)

        with tf.control_dependencies([emap]):
            (mn, va) = (tf.identity(ewa_mean), tf.identity(ewa_var))

        m1,v1 = tf.cond(is_training, lambda: (mean.assign(mn), variance.assign(va)), lambda: (mean, variance) ,name="bn_cond1")

        with tf.control_dependencies([m1, v1]):
            m2, v2 = tf.cond(is_training, lambda: (m, v), lambda: (mean, variance), name="bn_cond")

        norm = tf.nn.batch_normalization(tensor_in, m2, v2, beta, gamma, epsilon, name=name)

        return norm


def conv2d(x, n_filters,
           k_h=5, k_w=5,
           stride_h=2, stride_w=2,
           stddev=0.02,
           bias=False,
           batch_norm=False,
           is_training=None,
           activation=lambda x: x,
           padding='SAME',
           name="Conv2D"):
    """2D Convolution with options for kernel size, stride, and init deviation.

    Parameters
    ----------
    x : Tensor
        Input tensor to convolve.
    n_filters : int
        Number of filters to apply.
    k_h : int, optional
        Kernel height.
    k_w : int, optional
        Kernel width.
    stride_h : int, optional
        Stride in rows.
    stride_w : int, optional
        Stride in cols.
    stddev : float, optional
        Initialization's standard deviation.
    batch_norm : bool, optional
        Whether or not to apply batch normalization
    is_training: Tensor (bool)
        True if we are training (needed for batch normalisation)
    bias: bool, optional
        Whether to add bias
    activation : arguments, optional
        Function which applies a nonlinearity
    padding : str, optional
        'SAME' or 'VALID'
    name : str, optional
        Variable scope to use.

    Returns
    -------
    x : Tensor
        Convolved input.
    """
    with tf.variable_scope(name):
        w = tf.get_variable(
            'w', [k_h, k_w, x.get_shape()[-1], n_filters],
            initializer=tf.truncated_normal_initializer(stddev=stddev))

        conv = tf.nn.conv2d(x, w, strides=[1, stride_h, stride_w, 1], padding=padding)

        if bias:
            b = tf.get_variable(
                'b', [n_filters],
                initializer=tf.truncated_normal_initializer(stddev=stddev))
            conv = conv + b

        if batch_norm:
            numsteps = tf.to_float(tf.get_default_graph().as_graph_element("numsteps:0"))
            #momentum =  tf.minimum(0.99, tf.divide(tf.add(1.0,numsteps), tf.add(10.0,numsteps)))
            momentum = 0.99

            conv = tf.layers.batch_normalization(conv, training=is_training, center=True, scale=True, renorm = False, momentum=momentum)
            #if name=='convMel':
            #    conv = tf.Print(conv,[is_training, tf.get_default_graph().as_graph_element("convMel/batch_normalization/moving_mean:0")])

            #conv = batch_normalize(conv, is_training)
            #if name=='convMel':
            #    conv = tf.Print(conv,[is_training, tf.get_default_graph().as_graph_element("convMel/batch_norm/convMel/batch_norm/moments/Squeeze/ewma:0")])

        return activation(conv)


def linear(x, n_units,
           stddev=0.02,
           bias=False,
           batch_norm=False,
           activation=lambda x: x,
           name='Linear'):
    """Fully-connected network.

    Parameters
    ----------
    x : Tensor
        Input tensor to the network.
    n_units : int
        Number of units to connect to.
    name : str, optional
        Variable scope to use.
    stddev : float, optional
        Initialization's standard deviation.
    activation : arguments, optional
        Function which applies a nonlinearity
    bias : bool, optional
        Whether to apply bias (default False)

    Returns
    -------
    x : Tensor
        Fully-connected output.
    """
    shape = x.get_shape().as_list()

    with tf.variable_scope(name):
        matrix = tf.get_variable('w', [shape[1], n_units], tf.float32,
                                 tf.random_normal_initializer(stddev=stddev))
        matrix = tf.matmul(x, matrix)

        if bias:
            b = tf.get_variable(
                'b', [n_units],
                initializer=tf.truncated_normal_initializer(stddev=stddev))
            matrix = matrix + b

        if batch_norm:
            matrix = batch_normalize(matrix)

        return activation(matrix)


import tensorflow as tf

all_labels = []

flags = tf.app.flags
flags.DEFINE_integer('n_epochs', 500, 'Number of epochs to run trainer.') # 40
flags.DEFINE_integer('train_batch_size', 128, 'Batch size.')
flags.DEFINE_integer('test_batch_size', 128, 'Batch size.')
flags.DEFINE_integer('fetch_buffer_size', 3000, 'Fetch buffer size for shuffling.')
flags.DEFINE_integer('num_samples', None, 'Number of samples to use for training  and testing.')
#flags.DEFINE_string('train_dir', 'D:/Users/matic/Research/Session Segmentation/tensorflow/trained_model', 'Directory where to write event logs and checkpoint.')
#flags.DEFINE_string('log_dir','D:/Users/matic/Research/Session Segmentation/tensorflow/log','Root folder of DB samples') # defined in read_data
#flags.DEFINE_string('DBRoot','D:/Users/matic/Research/Databases/session segmentation','Root folder of DB samples') # defined in read_data
#flags.DEFINE_string('train_dir', '/output/trained_model', 'Directory where to write event logs and checkpoint.')
flags.DEFINE_string('log_dir','/output/log','Root folder of DB samples') # defined in read_data
flags.DEFINE_string('model_dir','/output/log/runs6jtvqdf','model to load') # defined in read_data
flags.DEFINE_string('DBRoot','/data','Root folder of DB samples') # defined in read_data
flags.DEFINE_string('all_labels','','All labels from dataset') # defined in read_data
flags.DEFINE_integer('do_mel',3,'Read mel spectrum vs spectrum.')
flags.DEFINE_integer('num_timesteps',2,'Length of spectrum (time steps)')
flags.DEFINE_integer('test_step_size',1,'Step over frames when testing')
flags.DEFINE_integer('num_freqs',0,'Width of spectrum (frequency bins)')
FLAGS = tf.app.flags.FLAGS
